// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBPRlo1mU0pCHBZPAWyHbtMsn794lJzs9o",
    authDomain: "taller-geocalizacion.firebaseapp.com",
    databaseURL: "https://taller-geocalizacion.firebaseio.com",
    projectId: "taller-geocalizacion",
    storageBucket: "taller-geocalizacion.appspot.com",
    messagingSenderId: "823906252082",
    appId: "1:823906252082:web:81269830936b2707"
  },
  googleMapsKey: "AIzaSyDaRpNWofVuk9qMHBSfM6jYhnnoV0yDJMc"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
